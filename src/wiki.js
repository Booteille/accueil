/* eslint-disable global-require */
/* eslint-disable import/no-commonjs */
function formatWiki(env, data) {
  const wiki = {};
  const json = (data !== undefined && data.length > 0)
    ? data
    : require('../public/wiki.json'); // eslint-disable-line

  for (let i = 0; i < json.length; i += 1) {
    const front = json[i].content.match(/^---([\s\S]*)---/m)
      ? json[i].content.match(/^---([\s\S]*)---/m)[0]
      : '';
    const lg = front.match(/lang: ([a-zA-Z_]+)/)
      ? front.match(/lang: ([a-zA-Z_]+)/)[1]
      : '';

    const date = front.match(/date: ([0-9]{2}-[0-9]{2}-[0-9]{2})/)
      ? front.match(/date: ([0-9]{2}-[0-9]{2}-[0-9]{2})/)[1]
      : '';

    const type = front.match(/type: ([a-z]+)/)
      ? front.match(/type: ([a-z]+)/)[1]
      : '';

    let url = front.match(/url: (.+)/)
      ? front.match(/url: (.+)/)[1]
      : '';
    if (url === '' && type === 'news') {
      url = `${env.url}news#${date}`;
    }

    /* Format content */
    const content = json[i].content
      // Replace wiki relative links by local links
      .replace(/\]\(uploads\//gm, `](${env.url}wiki/`)
      // Remove front matter
      .replace(front, '');

    /* Extract data */
    const img = content.match(/^!\[.*\]\((.*?)\)/m)
      ? content.match(/^!\[.*\]\((.*?)\)/m)[1]
      : '';
    const text = content
      .replace(/^(!\[.*\]\((.*)\))/m, '') // remove img
      .replace(/^[\s]*\n+/m, ''); // remove spaces & newlines at the begining;

    if (/^news/.test(type)) { // news or newsletter
      // Publication need a title (else it is a draft)
      if (json[i].title !== '') {
        if (wiki[lg] === undefined) {
          wiki[lg] = {};
          wiki[lg][type] = {
            posts: {},
          };
        }
        wiki[lg][type].posts[date] = {};
        Object.assign(wiki[lg][type].posts[date], {
          img,
          url,
          title: json[i].title,
          text,
        });
      }
    }
  }

  return wiki;
}

module.exports = formatWiki;
